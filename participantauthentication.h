#ifndef PARTICIPANTAUTHENTICATION_H
#define PARTICIPANTAUTHENTICATION_H

#include "cigarasql.h"

#include <QDialog>
#include <QSqlQuery>
namespace Ui {
class ParticipantAuthentication;
}

class ParticipantAuthentication : public QDialog {
  Q_OBJECT

public:
  explicit ParticipantAuthentication(QDialog *parent = nullptr);
  ~ParticipantAuthentication();
  QSqlQuery query;

private slots:
  void on_pushButtonBarcode_clicked();

  void on_pushButtonSubmitID_clicked();

  //void on_pushButtonSurname_clicked();

  void on_pushButtonBack_clicked();

  void on_pushButtonTableNumber_clicked();

private:
  Ui::ParticipantAuthentication *ui;
  CigaraSql sqlObj;
};

#endif // PARTICIPANTAUTHENTICATION_H
