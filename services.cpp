#include "services.h"

Services::Services()
{

}

QString Services::  getRandomASCII(const qint32 &length, const qint32 &base)
{
    QTime time = QTime::currentTime();

    QRandomGenerator((quint32)time.msec());

    QString randomHex;

    for (int i = 0; i < length; i++) {

        quint32 n = QRandomGenerator::global()->generate() % base;
        randomHex.append(QString::number(n, base));
    }

    return randomHex.toUpper();
}

