#include "loginupdatepage.h"
#include "ui_loginupdatepage.h"
#include <QDialog>
#include <QMessageBox>
#include <mainwindow.h>

LoginUpdatePage::LoginUpdatePage(QDialog *parent)
    : QDialog(parent), ui(new Ui::LoginUpdatePage) {
  ui->setupUi(this);
}

LoginUpdatePage::~LoginUpdatePage() { delete ui; }

void LoginUpdatePage::on_pushButtonVerify_clicked() {
  QString email = ui->lineEditEmail->text();
  QString username = ui->lineEditUsername->text();
  QString password = ui->lineEditPassword->text();
  QString repassword = ui->lineEditNewpassword->text();

  if (password == repassword) {

    int status = sqlObj.loginInfo(email, username, password);

    if (status == -3)
      QMessageBox::warning(this, "Password Update", "Invalid Email ID");

    else if (status == -2)
      QMessageBox::warning(this, "Password Update", "Username Doesnt Match");

    else if (status == -1)
      QMessageBox::warning(this, "Password Update", "Password Not Updated");

    else if (status == 1)
      QMessageBox::warning(this, "Password Update", "Password Updated");

    else
      QMessageBox::warning(this, "Password Update", "Error");

  } else
    QMessageBox::warning(this, "Password Update", "Password Doesnt Match");
}

void LoginUpdatePage::on_pushButtonBack_clicked() {
  UpdatePage *back = new UpdatePage();
  back->show();
}
