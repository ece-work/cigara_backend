#ifndef CIGARASQL_H
#define CIGARASQL_H

#include <QString>
#include <QtSql>

class CigaraSql {
public:
  CigaraSql();
  bool connectDB();
  void closeDB();
  bool checkLogin(QString, QString);
  QSqlQuery participantInfo();
  QSqlQuery userInfo(QString);
  QSqlQuery allUserInfo();
  int loginInfo(QString, QString, QString);
  bool checkSurname(QString);
  QSqlQuery tableViewData(QString);
  bool updateParticipant(QString, QString, QString, QString);
  bool updateBarcode(QString, QByteArray,QString);
  QByteArray viewBarcode(QString email);
  bool insertParticipant(QMap<QString, QString>);
  bool RandomIDIsExist(QString RandomID);
  bool checkBarcode(QString barcodenumber);
  bool insertTableNumber(QString tableNumber, QString EmailID);

private:
  QSqlDatabase db;
};

#endif // CIGARASQL_H
