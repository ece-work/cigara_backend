#include "updatepage.h"
#include "ui_updatepage.h"
#include <QDesktopWidget>
#include <QDialog>
#include <mainwindow.h>

UpdatePage::UpdatePage(QDialog *parent)
    : QDialog(parent), ui(new Ui::UpdatePage) {
  ui->setupUi(this);
  resize(QDesktopWidget().availableGeometry(this).size() * 1);
}

UpdatePage::~UpdatePage() { delete ui; }

void UpdatePage::on_pushButtonLoginInformation_clicked() {
  LoginUpdatePage *UpdateLoginChanges = new LoginUpdatePage(this);
  UpdateLoginChanges->show();
  this->hide();
}

void UpdatePage::on_pushButtonPersonalInformation_clicked() {
  RegistrationPage *PersonalDetails = new RegistrationPage(this);
  PersonalDetails->show();
  this->hide();
}

void UpdatePage::on_pushButtonLogout_clicked() {
  close();
  MainWindow *page = new MainWindow();
  page->show();
}
