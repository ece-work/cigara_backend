#include "participantinfo.h"
#include "ui_participantinfo.h"
#include <QFont>
#include <QMessageBox>
#include <QPageSize>
#include <QPainter>
#include <QPdfWriter>
#include <QPrinter>
#include <QPushButton>
#include <QSqlTableModel>
#include <QStringListModel>
#include <QTextDocument>
#include <QtQrCode>
#include <QtQrCodePainter>
#include <adminpage.h>

ParticipantInfo::ParticipantInfo(QDialog *parent)
    : QDialog(parent), ui(new Ui::ParticipantInfo) {
  ui->setupUi(this);

  Uploadinfo();
}
ParticipantInfo::~ParticipantInfo() { delete ui; }

// Upload info button
void ParticipantInfo::Uploadinfo() {

  query = sqlObj.allUserInfo();
  QSqlQueryModel *model = new QSqlQueryModel();
  model->setQuery(query);
  ui->tableView->setModel(model);

  int num_rows = model->rowCount();

  int i = 0;
  while (i <= num_rows) {
    QPushButton *Button = new QPushButton("Send Email");
    ui->tableView->setIndexWidget(model->index(i, 8), Button);
    i++;
    // connect(Button, SIGNAL(clicked()), this, SLOT(SendEmail_clicked(i)));
  }
}

// TODO: get this running
// void ParticipantInfo::SendEmail_clicked(int i) {
//  qDebug() << "Sending Email...";
//  ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
//  int column = 8, row = i;
//  ui->tableView->selectRow(column);
//  QSqlQuery query;
//  ui->lineEdit->setText(query.value(row).toString());
//  qDebug() << query.value(i).toString();

//  void on_tableView_activated();
//  printBarcode("qwerty");
//}

void ParticipantInfo::on_tableView_activated(const QModelIndex &index) {
  ui->tableView->model()->flags(index) & Qt::ItemIsEditable;

  // qDebug()<<index;
  // int row=index.row();
  // int column=index.column();
  // qDebug()<<row<<column;
  QString value = ui->tableView->model()->data(index).toString();

  // qDebug()<<value;

  query = sqlObj.tableViewData(value);

  if (query.next()) {
    ui->lineEdit->setText(query.value(0).toString());
    ui->lineEditSurname->setText(query.value(1).toString());
    ui->lineEditEmail->setText(query.value(2).toString());
    ui->lineEditOnlinePay->setText(query.value(5).toString());
    ui->lineEditCompetitor->setText(query.value(6).toString());
    ui->lineEditPaid->setText(query.value(7).toString());
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);

  } else {
    qDebug() << "Query not working";
  }
}

void ParticipantInfo::on_pushButtonUpdate_clicked() {

  QString email = ui->lineEditEmail->text();
  QString OnlinePay = ui->lineEditOnlinePay->text();
  QString Competitor = ui->lineEditCompetitor->text();
  QString Paid = ui->lineEditPaid->text();

  if (sqlObj.updateParticipant(email, OnlinePay, Competitor, Paid)) {
    Uploadinfo();
    QMessageBox::information(this, " Update info", "Information is updated");
  } else {
    QMessageBox::information(this, " Update info",
                             "Information is not updated");
  }
  // Uploadinfo();
}

// send email
void ParticipantInfo::on_pushButtonSendEmail_clicked() {

  QString email = ui->lineEditEmail->text();
  if (email.isNull() || email.isEmpty()) {
    QMessageBox::warning(this, "Send Email Error",
                         "EmailID is NULL. Please enter EmailID.");
  } else {

    if (printBarcode(email)) {
      QMessageBox::information(this, "Generate Barcode",
                               "Barcode is generated");
      qDebug() << "PrintBarCode function finished";

    } else {
      QMessageBox::warning(this, "Generate Barcode",
                           "Barcode is not generated");
    }
  }

  Uploadinfo();
}

// Barcode generation
bool ParticipantInfo::printBarcode(QString barcodeText) {

  bool status = false;
  /* SEL 28-08-2019 : Replace bardcode with Qr Code */
  QtQrCode m_qrCode;
  // QtQrCodePainter *QrPainter =  new QtQrCodePainter(100);
  QtQrCodePainter QrPainter;

  QString filePath = ".";

  // QString filePath = QFileDialog::getExistingDirectory(this, tr("Select
  // Location to save Barcode file"), QDir::currentPath(),
  // QFileDialog::ShowDirsOnly);
  /*if(filePath.isNull() || filePath.isEmpty()){
      QMessageBox::warning(this, "Error","Location to save file not selected");
      return status;
  }*/

  /* *****-----*****     Calculating PassportID      *****-----***** */
  QString RandomID;
  do {
    RandomID = Services::getRandomASCII(8, 10);
    qDebug() << " Random ID is ---------------> " << RandomID;
  } while (sqlObj.RandomIDIsExist(RandomID));

  /* *****-----***** Printing the QrCode to PDF File *****-----***** */
  /* Crear Qr Printer */
  QPrinter printer;
  /* setthe printer device to print in */
  printer.setOutputFileName(barcodeText + ".pdf");
  /* Creat the painter */
  QPainter painter(&printer);
  /* Adapt the font for drawing */
  QFont QrCodeFont = QFont("QR Code", 200, QFont::Normal);
  QrCodeFont.setLetterSpacing(QFont::AbsoluteSpacing, 0.0);
  painter.setFont(QrCodeFont);
  /* SEL 28-08-2019 : Replace bardcode with Qr Code */
  m_qrCode.setData(RandomID.toUtf8());
  QrPainter.paint(m_qrCode, painter, 300, 300);
  painter.end();

  QMessageBox::information(this, "Generate Qr Code",
                           "QrCode File " + barcodeText + ".pdf Generated");

  QFile file(filePath + "/" + barcodeText + ".pdf");
  qDebug() << "filepath : " << file;

  if (!file.open(QIODevice::ReadOnly)) {
    status = false;
    return status;
  } else {

    QByteArray byteArray = file.readAll();
    qDebug() << "file : " << byteArray;
    /* Update Qr Code on the Database */
    if (sqlObj.updateBarcode(barcodeText, byteArray, RandomID)) {
      QMessageBox::information(this, "Barcode",
                               "Barcode File " + barcodeText +
                                   ".pdf is inserted to database");

      qDebug() << "Sending Email...";
      QString subject = "Clubmareva Registration Code";
      QString body = "Registration Code as PDF is attached.\n\nPlease bring "
                     "the Code with you for the Registration.\n\n\nWelcome the "
                     "CSWC Event 2019!\n\nTeam Clubmareva.";
      QStringList files = {filePath + "/" + barcodeText + ".pdf"};
      smtpObj.sendMail("zeljana@clubmareva.com", barcodeText, subject, body,
                       files);
      status = true;
      //            qDebug() << "Starting Event Loop";
      //            QEventLoop loop;
      //            QObject::connect(&smtpObj, SIGNAL(endLoop()), &loop,
      //            SLOT(quit())); loop.exec();
      //            QCoreApplication::processEvents();

    } else {
      QMessageBox::information(this, "Barcode",
                               "Barcode File " + barcodeText +
                                   ".pdf is not inserted to database");
      status = false;
    }

    QByteArray array = sqlObj.viewBarcode(barcodeText);
    qDebug() << "Barcode : " << array;
  }

  return status;
}

QString ParticipantInfo::encodeBarcode(QString code) {
  qDebug() << "Encoding Email...";
  QString encoded;
  encoded.prepend(
      QChar(codeToChar(CODE128_C_START))); // Start set with B Code 104
  encoded.append(code);
  encoded.append(QChar(calculateCheckCharacter(code)));
  encoded.append(QChar(codeToChar(CODE128_STOP))); // End set with Stop Code 106
  return encoded;
}

int ParticipantInfo::calculateCheckCharacter(QString code) {
  QByteArray encapBarcode(code.toUtf8()); // Convert code to utf8

  // Calculate check character
  long long sum =
      CODE128_C_START; // The sum starts with the B Code start character value
  int weight = 1;      // Initial weight is 1

  foreach (char ch, encapBarcode) {
    int code_char = charToCode((int)ch); // Calculate character code
    sum += code_char * weight;           // add weighted code to sum
    weight++;                            // increment weight
  }

  int remain = sum % 103; // The check character is the modulo 103 of the sum

  // Calculate the font integer from the code integer
  if (remain >= 95)
    remain += 105;
  else
    remain += 32;

  return remain;
}

int ParticipantInfo::codeToChar(int code) { return code + 105; }

int ParticipantInfo::charToCode(int ch) { return ch - 32; }

// Back button
void ParticipantInfo::on_pushButtonBack_clicked() {
  AdminPage *back = new AdminPage();
  const QIcon WindowIcon(":/EU Platform icon.png");
  back->setWindowIcon(WindowIcon);
  back->show();
  this->hide();
}
