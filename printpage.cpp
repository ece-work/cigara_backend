#include "printpage.h"
#include "ui_printpage.h"
#include <QAbstractTableModel>
#include <QDesktopWidget>
#include <QDialog>
#include <QFont>
#include <QMessageBox>
#include <QPainter>
#include <QPdfWriter>
#include <QPrinter>
#include <QSqlTableModel>
#include <QtPrintSupport/QPrinter>
#include <QtSql>
#include <adminpage.h>

PrintPage::PrintPage(QDialog *parent)
    : QDialog(parent), ui(new Ui::PrintPage)

{
  ui->setupUi(this);

  QSqlQueryModel *model = new QSqlQueryModel();
  model->setQuery(sqlObj.participantInfo());

  // ui->listView->setModel(model);
  ui->comboBox->setModel(model);
}
PrintPage::~PrintPage() { delete ui; }

void PrintPage::on_pushButtonPrintUserInfo_clicked() {
  QString id = selectedItem;
  QPrinter printer;

  QString CSVfile;
  CSVfile = QFileDialog::getSaveFileName(
      this, tr("Open PDF file"), "/Desktop/CigaraParticipiants2019INFO.pdf",
      tr("PDF Files (*.pdf)"));

  printer.setOutputFileName(CSVfile);
  QPdfWriter pdfWriter("");

  pdfWriter.setPageSize(QPageSize(QPageSize::A4));
  QPainter painter(&pdfWriter);

  QSqlQuery query = sqlObj.userInfo(id);

  if (query.next()) {
    qDebug() << "Information of" << query.value(0).toString();

    if (painter.begin(&printer)) {
      QStringList details = {"Name",      "Surname",     "EmailID",
                             "Country",   "PassportID",  "Competitor",
                             "OnlinePay", "Paid",        "EmailSent",
                             "BarCode",   "TableNumber", "EnterCompetition"};

      int x_axis = 30, y_axis = 30, increment = 30, column_count = 11,
          colon_x_axis = 145, query_x_axis = 160;

      for (int column = 0; column <= column_count; column++) {
        painter.drawText(x_axis, y_axis, details[column]);
        painter.drawText(colon_x_axis, y_axis, ":");
        painter.drawText(query_x_axis, y_axis, query.value(column).toString());
        y_axis += increment;
      }
      QMessageBox::information(this, " User Information",
                               " " + id + ".pdf is Generated");
    } else
      QMessageBox::warning(
          this, "Print Information",
          "failed to open file, is it writable?"); // failed to open file
  } else
    QMessageBox::information(this, " Print Information",
                             "Information Not Found");
}

void PrintPage::on_pushButtonPrintAllUserInfo_clicked() {
  QPrinter printer;
  QPdfWriter pdfWriter("");

  printer.setOutputFileName("C://information.pdf");
  pdfWriter.setPageSize(QPageSize(QPageSize::A4));
  QPainter painter(&pdfWriter);

  QSqlQuery query = sqlObj.allUserInfo();
  if (!painter.begin(&printer)) { // failed to open file
    qWarning("failed to open file, is it writable?");
  } else {
    qDebug() << "All user info";
    QStringList list;

    while (query.next()) {
      QStringList details = {"Name",      "Surname",     "EmailID",
                             "Country",   "PassportID",  "Competitor",
                             "OnlinePay", "Paid",        "EmailSent",
                             "BarCode",   "TableNumber", "EnterCompetition"};
      int x_axis = 30, y_axis = 30, increment = 30, column_count = 11,
          colon_x_axis = 145, query_x_axis = 160;

      for (int column = 0; column <= column_count; column++) {
        if (column != 9) {
          painter.drawText(x_axis, y_axis, details[column]);
          painter.drawText(colon_x_axis, y_axis, ":");
          painter.drawText(query_x_axis, y_axis,
                           query.value(column).toString());
          y_axis += increment;
        }
      }
      printer.newPage();
    }
    QMessageBox::information(this, " All User Information",
                             " Information.pdf is Generated");
    painter.end();
  }
}

void PrintPage::on_pushButtonBack_clicked() {
  AdminPage *back = new AdminPage();
  back->show();
  this->hide();
}

void PrintPage::on_comboBox_currentIndexChanged(const QString &arg1) {
  selectedItem = arg1;
}
