#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "adminpage.h"
#include "updatepage.h"
#include "cigarasql.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
  Q_OBJECT
public:
  bool database();

public:
  explicit MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

private slots:
  void on_pushButton_Login_clicked();

private:
  Ui::MainWindow *ui;
  AdminPage *logout;
  CigaraSql sqlObj;
};

#endif // MAINWINDOW_H
