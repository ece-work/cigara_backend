#ifndef REGISTRATIONPAGE_H
#define REGISTRATIONPAGE_H
#include "cigarasql.h"

#include <QDialog>

namespace Ui {
class RegistrationPage;
}

class RegistrationPage : public QDialog {
  Q_OBJECT

public:
  explicit RegistrationPage(QDialog *parent = nullptr);
  ~RegistrationPage();

private slots:
  void on_pushButtonsubmit_clicked();

  void on_pushButtonBack_clicked();

private:
  Ui::RegistrationPage *ui;
  // UpdatePage *NewUpdatePage;
  CigaraSql sqlObj;
};

#endif // REGISTRATIONPAGE_H
