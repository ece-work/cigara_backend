#ifndef UPDATEPAGE_H
#define UPDATEPAGE_H

#include <QDialog>
#include <loginupdatepage.h>
#include <registrationpage.h>

namespace Ui {
class UpdatePage;
}

class UpdatePage : public QDialog {
  Q_OBJECT

public:
  explicit UpdatePage(QDialog *parent = nullptr);
  ~UpdatePage();

private slots:

  void on_pushButtonPersonalInformation_clicked();

  void on_pushButtonLoginInformation_clicked();

  void on_pushButtonLogout_clicked();

private:
  Ui::UpdatePage *ui;
};

#endif // UPDATEPAGE_H
