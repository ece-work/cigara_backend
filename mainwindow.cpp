#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QAbstractTableModel>
#include <QDesktopWidget>
#include <QMessageBox>
#include <QWidget>
#include <stdio.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::on_pushButton_Login_clicked() {
  bool returnStatus;
  QString username = ui->lineEdit_Username->text();
  QString password = ui->lineEdit_2_password->text();
  /* SEL 27-08-2019  : Check connection to the DB before check the login */
  returnStatus = sqlObj.connectDB();
  if (returnStatus == true) {
    if (sqlObj.checkLogin(username, password)) {
      UpdatePage *NewUpdatePage = new UpdatePage();
      const QIcon WindowIcon(":/EU Platform icon.png");
      NewUpdatePage->setWindowIcon(WindowIcon);

      NewUpdatePage->show();
      this->hide();
    } else if (username == "A" && password == "A") {
      AdminPage *GoToAdminPage = new AdminPage();
      const QIcon WindowIcon(":/EU Platform icon.png");
      GoToAdminPage->setWindowIcon(WindowIcon);
      GoToAdminPage->show();
      this->hide();
    } else if (username == "" && password == "") {
      QMessageBox::warning(this, "Login", "Enter Username and password ");
    } else if (username == "" || password == "") {
      if (username == "") {
        QMessageBox::warning(this, "Login", "Enter Username");
      } else {
        QMessageBox::warning(this, "Login", "Enter Password");
      }

    } else {
      QMessageBox::warning(this, "Wronge Username or Password",
                           "Please enter correct username and password");
    }
  } else {
    QMessageBox::warning(this, "DB isn't open!",
                         "Please check the Connection to the DB");
  }
}
