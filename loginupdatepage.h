#ifndef LOGINUPDATEPAGE_H
#define LOGINUPDATEPAGE_H

#include "cigarasql.h"

#include <QDialog>

namespace Ui {
class LoginUpdatePage;
}

class LoginUpdatePage : public QDialog {
  Q_OBJECT

public:
  explicit LoginUpdatePage(QDialog *parent = nullptr);
  ~LoginUpdatePage();

private slots:
  void on_pushButtonVerify_clicked();

  void on_pushButtonBack_clicked();

private:
  Ui::LoginUpdatePage *ui;
  CigaraSql sqlObj;
};

#endif // LOGINUPDATEPAGE_H
