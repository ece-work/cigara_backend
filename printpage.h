#ifndef PRINTPAGE_H
#define PRINTPAGE_H

#include "cigarasql.h"

#include <QDialog>

namespace Ui {
class PrintPage;
}

class PrintPage : public QDialog {
  Q_OBJECT

public:
  explicit PrintPage(QDialog *parent = nullptr);
  ~PrintPage();
  QString selectedItem;

private slots:
  void on_pushButtonPrintUserInfo_clicked();

  void on_pushButtonPrintAllUserInfo_clicked();

  void on_pushButtonBack_clicked();

  void on_comboBox_currentIndexChanged(const QString &arg1);

private:
  Ui::PrintPage *ui;
  CigaraSql sqlObj;
};

#endif // PRINTPAGE_H
