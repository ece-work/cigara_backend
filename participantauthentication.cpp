#include "participantauthentication.h"
#include "ui_participantauthentication.h"
#include <QDialog>
#include <QMessageBox>
#include <adminpage.h>

ParticipantAuthentication::ParticipantAuthentication(QDialog *parent)
    : QDialog(parent), ui(new Ui::ParticipantAuthentication) {
  ui->setupUi(this);
}

ParticipantAuthentication::~ParticipantAuthentication() { delete ui; }

void ParticipantAuthentication::on_pushButtonBarcode_clicked() {
    QString barcode = ui->lineEdit->text();
    barcode = barcode.trimmed();
    if(barcode.isNull() || barcode.isEmpty()){
        QMessageBox::information(this, "Participant Authentication", "Please enter barcode");
        return;
    }

      if (sqlObj.checkBarcode(barcode)) {

    QMessageBox::information(this, "Participant Authentication",
                             "Barcode matched");

  } else {
    QMessageBox::information(this, "Participant Authentication",
                             "Information Not Found");
  }
  sqlObj.closeDB();
}


void ParticipantAuthentication::on_pushButtonSubmitID_clicked() {
  QString id = ui->lineEditID->text();
  id = id.trimmed();
  if(id.isNull() || id.isEmpty()){
      QMessageBox::information(this, "Participant Authentication", "Please enter emailID");
      return;
  }
  query = sqlObj.userInfo(id);
  if (query.next()) {
    QMessageBox::information(this, "Participant Authentication", "EmailID matched");

  } else {
    QMessageBox::information(this, "Participant Authentication",
                             "Information Not Found");
  }
  sqlObj.closeDB();
}

//void ParticipantAuthentication::on_pushButtonSurname_clicked() {
//  QString surname = ui->lineEditSurname->text();

//  if (sqlObj.checkSurname(surname)) {
//    ui->lineEditID->setText(query.value(2).toString());
//    ui->lineEditSurname->setText(query.value(1).toString());
//    QMessageBox::information(this, "Participant Authentication",
//                             "Surname matched");

//  } else {
//    QMessageBox::information(this, "Participant Authentication",
//                             "Surname doesnt match");
//  }
//}

void ParticipantAuthentication::on_pushButtonBack_clicked() {
  AdminPage *back = new AdminPage();
  back->show();
  this->hide();
}

void ParticipantAuthentication::on_pushButtonTableNumber_clicked()
{
    QString tableNumber = ui->lineEditTableNumber->text();
    if(tableNumber.isNull() || tableNumber.isEmpty()){
        QMessageBox::information(this, "Participant Authentication", "Please enter tableNumber");
        return;
    }

    QString emailID = ui->lineEditID->text();
    if(emailID.isNull() || emailID.isEmpty()){
        QMessageBox::information(this, "Participant Authentication", "Please enter emailID for this table number");
        return;
    }

    if (sqlObj.insertTableNumber(tableNumber, emailID)) {
      QMessageBox::information(this, "Participant Authentication", "Table Number inserted");

    } else {
      QMessageBox::information(this, "Participant Authentication",
                               "Table Number not inserted, please check EmailID");
    }
    sqlObj.closeDB();


}
