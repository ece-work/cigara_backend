#include "cigarasql.h"
#include <QMessageBox>
#include <QSqlError>
#include <QSqlTableModel>
#include <QtSql>

CigaraSql::CigaraSql() {}

bool CigaraSql::connectDB() {
  bool SQLerr = true;
  QString db_connectionName = QString("CigaraDB Connection");
  db = QSqlDatabase::addDatabase("QODBC3", db_connectionName);
  //  db.setHostName("92.79.180.59");
  //  db.setPort(106);
  db.setHostName("localhost/SQLEXPRESS");
  db.setDatabaseName("cigraproject_database");
  db.setUserName("ServerTesting");
  db.setPassword("Welcome123$");

  if (!db.open()) {

    SQLerr = db.lastError().isValid();
    qInfo() << "DB Error: " << db.lastError();
    db = QSqlDatabase();
    QSqlDatabase::removeDatabase(db_connectionName);

  } else {
    /* SEL 27-08-2019 L Open the desired data base */
    QSqlDatabase db = QSqlDatabase::database(db_connectionName);
    SQLerr = db.open();
    qInfo() << "DB Runs: ";
  }
  return SQLerr;
}

void CigaraSql::closeDB() {
  //  QString db_connectionName = QString("CigaraDB Connection");
  QSqlDatabase db = QSqlDatabase::database();
  db.close();

  //  QSqlDatabase::removeDatabase(db_connectionName);
}

bool CigaraSql::checkLogin(QString user, QString pwd) {

  bool login = false;

  /* SEL 27/08/2019 : Remove connection from this part of the function */

  // connectDB();

  QSqlQuery query(db);

  query.prepare("select Username, Password from Login where Username = "
                ":Username and Password = :Password");
  query.bindValue(":Username", user);
  query.bindValue(":Password", pwd);
  query.exec();
  if (query.next()) {
    QString name = query.value(0).toString();
    qDebug() << name << "logged in...";
    login = true;
  }

  return login;
}

int CigaraSql::loginInfo(QString email, QString username, QString pwd) {

  int status = 0;

  if (!db.open()) {
    connectDB();
  }

  QSqlQuery query1(db);
  QSqlQuery query2(db);

  query1.prepare("select * from Login where EmailID = :EmailID");
  query1.bindValue(":EmailID", email);
  query1.exec();

  while (query1.next()) {

    if (query1.numRowsAffected() != 0) {

      QString name = query1.value(1).toString();

      if (username == name) {

        query2.prepare(
            "update Login set Password = :Password where EmailID = :EmailID");
        query2.bindValue(":Password", pwd);
        query2.bindValue(":EmailID", email);
        if (query2.exec()) {
            if (query2.numRowsAffected() >= 1) {
                status = 1;
            }
            else {
                qDebug() << "No rows updated";
                status = 0;
            }
        } else
          status = -1;

      } else
        status = -2;

    } else
      status = -3;
  }

  return status;
}

bool CigaraSql::checkSurname(QString surname) {

  bool status = false;

  if (!db.open()) {
    connectDB();
  }

  QSqlQuery query(db);
  query.prepare("select * from ParticipantDetails where Surname =:Surname");
  query.bindValue(":Surname", surname);
  query.exec();

  if (query.next()) {
    status = true;
  }

  return status;
}

QSqlQuery CigaraSql::participantInfo() {

  if (!db.open()) {
    connectDB();
  }

  QSqlQuery query(db);
  query.prepare("select EmailId from ParticipantDetails");
  query.exec();

  return query;
}

QSqlQuery CigaraSql::userInfo(QString id) {

  if (!db.open()) {
    connectDB();
  }

  QSqlQuery query(db);
  query.prepare("select * from ParticipantDetails where EmailID=:EmailID");
  query.bindValue(":EmailID", id);
  query.exec();

  return query;
}

QSqlQuery CigaraSql::allUserInfo() {

  if (!db.open()) {
    connectDB();
  }

  QSqlQuery query(db);
  query.prepare("select * from ParticipantDetails");
  query.exec();

  return query;
}

QSqlQuery CigaraSql::tableViewData(QString value) {

  if (!db.open()) {
    connectDB();
  }

  QSqlQuery query(db);
  query.prepare("select * from ParticipantDetails where Name='" + value +
                "' or Surname='" + value + "' or EmailID='" + value +
                "'or  Country='" + value + "'or PassportID='" + value +
                "'or Competitor='" + value + "'or OnlinePay='" + value +
                "'or Paid='" + value + "'or EmailSent='" + value +
                "' or TableNo='" + value + "'or EnterCompetition='" + value +
                "'");
  query.exec();

  return query;
}

bool CigaraSql::updateParticipant(QString email, QString OnlinePay,
                                  QString Competitor, QString Paid) {

  bool status = false;

  if (!db.open()) {
    connectDB();
  }

  QSqlQuery query(db);
  query.prepare("update ParticipantDetails set OnlinePay =:OnlinePay, "
                "Competitor=:Competitor, Paid=:Paid where EmailID=:EmailID");
  query.bindValue(":OnlinePay", OnlinePay);
  query.bindValue(":Competitor", Competitor);
  query.bindValue(":Paid", Paid);
  query.bindValue(":EmailID", email);

  if (query.exec()) {
      if (query.numRowsAffected() >= 1) {
        status = true;
      } else {
          qDebug() << "No rows updated";
          status = false;
      }
  }

  return status;
}

bool CigaraSql::insertParticipant(QMap<QString, QString> Participant) {

  bool status = false;

  if (!db.open()) {
    connectDB();
  }

  QSqlQuery query(db);
  query.prepare(
      "insert into ParticipantDetails (Name, Surname, EmailID, Country, "
      "PassportID, Competitor)"
      "values (:Name,:Surname,:EmailID,:Country,:PassportID,:Competitor)");

  query.bindValue(":Name", Participant["name"]);
  query.bindValue(":Surname", Participant["surname"]);
  query.bindValue(":EmailID", Participant["email"]);
  query.bindValue(":Country", Participant["country"]);
  query.bindValue(":PassportID", Participant["passportID"]);
  query.bindValue(":Competitor", Participant["competitor"]);

  if (query.exec()) {
    status = true;
  }

  return status;
}

bool CigaraSql::updateBarcode(QString email, QByteArray Barcode , QString PassportID) {

  bool status = false;

  if (!db.open()) {
    connectDB();
  }

  QSqlQuery query(db);
  query.prepare("update ParticipantDetails set BarCode =:BarCode , PassportID "
                "=:PassportID where EmailID = :EmailID");

  query.bindValue(":BarCode", Barcode);
  query.bindValue(":EmailID", email);
  query.bindValue(":PassportID", PassportID);
  if (query.exec()) {
      if (query.numRowsAffected() >= 1) {
        status = true;
      } else {
          qDebug() << "No rows updated";
          status = false;
      }
  }

  return status;
}

QByteArray CigaraSql::viewBarcode(QString email) {

  QByteArray barcode;

  if (!db.open()) {
    connectDB();
  }

  QSqlQuery query(db);
  query.prepare(
      "SELECT * FROM ParticipantDetails where EmailID = :EmailID");
  qDebug() << " Email sent is : " << email;
  query.bindValue(":EmailID", email);

  if (query.exec()) {
      if(query.next()){
          barcode = query.value(9).toByteArray();
      }
      else {
          qDebug() << "No data in query";
      }

  } else {
      qDebug() << "Error in query";

    }

  return barcode;
}

bool CigaraSql::RandomIDIsExist(QString RandomID) {
  bool returnStatus;
  QSqlQuery query(db);
  query.prepare("select PassportID from ParticipantDetails where PassportID = "
                ":PassportID ");
  query.bindValue(":PassportID", RandomID);
  query.exec();
  if (!query.next()) {
    qDebug() << "Random ID :  " << RandomID << " Is not used before ... ";
    returnStatus = false;
  } else {
    returnStatus = true;
  }
  return returnStatus;
}


bool CigaraSql::checkBarcode(QString barcodenumber) {

  bool status = false;

  if (!db.open()) {
    connectDB();
  }

  QSqlQuery query(db);
  query.prepare("select * from ParticipantDetails where PassportID =:barcodenumber");
  query.bindValue(":barcodenumber", barcodenumber);
  query.exec();

  if (query.next()) {
    status = true;
  }

  return status;
}


bool CigaraSql::insertTableNumber(QString tableNumber, QString EmailID) {

  bool status = false;

  if (!db.open()) {
    connectDB();
  }

  QSqlQuery query(db);

  query.prepare("update ParticipantDetails set TableNo =:tableNumber where EmailID = :EmailID");
  query.bindValue(":tableNumber", tableNumber);
  query.bindValue(":EmailID", EmailID);
  if (query.exec()) {
      if (query.numRowsAffected() >= 1) {
        status = true;
        } else {
          qDebug() << "No rows updated";
          status = false;
      }
    }
  return status;

}
