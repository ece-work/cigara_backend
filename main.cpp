#include "mainwindow.h"
#include <QApplication>
#include <QIcon>

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);
  MainWindow w;
  w.setWindowTitle(QObject::tr("EUPLTF - Cigara Tool"));
  w.show();

  const QIcon WindowIcon(":/EU Platform icon.png");
  w.setWindowIcon(WindowIcon);

  return a.exec();
}
