#include "adminpage.h"
#include "ui_adminpage.h"
#include <QAbstractTableModel>
#include <QDesktopWidget>
#include <QDialog>
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QSqlTableModel>
#include <QtSql>
#include <mainwindow.h>
#include <stdio.h>
AdminPage::AdminPage(QDialog *parent) : QDialog(parent), ui(new Ui::AdminPage) {
  ui->setupUi(this);
}

AdminPage::~AdminPage() { delete ui; }

// Participant Information
void AdminPage::on_pushButtonParticipantInformation_clicked() {
  ParticipantInfo *Information = new ParticipantInfo();

  const QIcon WindowIcon(":/EU Platform icon.png");
  Information->setWindowIcon(WindowIcon);

  Information->show();
  this->hide();
}

// Download Excel
void AdminPage::on_pushButtonDownladExcel_clicked() {

  QString CSVfile;
  CSVfile = QFileDialog::getSaveFileName(this, tr("Open CSV file"),
                                         "/Desktop/CigaraParticipiants2019.csv",
                                         tr("CSV Files (*.csv)"));

  QFile CSVFileQF(CSVfile);

  if (!CSVFileQF.open(QFile::WriteOnly | QFile::Text)) {
    QMessageBox::information(this, "Download excel", "File is not open");
  } else {

    QTextStream out(&CSVFileQF);
    QSqlQuery query = sqlObj.allUserInfo();

    out << "Username,"
        << "Surname,"
        << "EmailId,"
        << "Country,"
        << "Passport ID,"
        << "Competitor,"
        << "Online pay,"
        << "Paid,"
        << "Email Sent,"
        << "Bar Code,"
        << "Table Number,"
        << "Enter Competition\n";
    int column_count = 11;
    while (query.next()) {
      for (int column = 0; column <= column_count; column++) {
        if (column != 9)
          out << query.value(column).toString() << ",";
      }
      out << "\n";
    }
    QMessageBox::information(this, "Download excel",
                             "Participantinfo csv file is created");
    CSVFileQF.close();
  }
}

// Participant authentication
void AdminPage::on_pushButtonParticipantAuthentication_clicked() {
  ParticipantAuthentication *GoToParticipantAuthentication =
      new ParticipantAuthentication();
  const QIcon WindowIcon(":/EU Platform icon.png");
  GoToParticipantAuthentication->setWindowIcon(WindowIcon);

  GoToParticipantAuthentication->show();
  this->hide();
}

// print the information
void AdminPage::on_pushButtonPrintInformation_clicked() {
  PrintPage *GoToPrintInformation = new PrintPage();

  const QIcon WindowIcon(":/EU Platform icon.png");
  GoToPrintInformation->setWindowIcon(WindowIcon);
  GoToPrintInformation->show();
  this->hide();
}

// Logout to to display MainWindow
void AdminPage::on_pushButtonLogout_clicked() {
  close();
  MainWindow *page = new MainWindow();

  const QIcon WindowIcon(":/EU Platform icon.png");
  page->setWindowIcon(WindowIcon);

  page->show();
}
