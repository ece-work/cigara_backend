#ifndef PARTICIPANTINFO_H
#define PARTICIPANTINFO_H
#include "cigarasql.h"
#include "smtp.h"

#include <QObject>
#include <QSqlQuery>
#include <QtPrintSupport>
#define CODE128_C_START 99
#define CODE128_STOP 101
#include <QDialog>
#include <QSqlQueryModel>
#include "services.h"
namespace Ui {
class ParticipantInfo;
}

class ParticipantInfo : public QDialog {
  Q_OBJECT
public:
  // bool SendEmail();
  void Uploadinfo();

  QSqlQuery query;

  /** Prints the text as a barcode */
  bool printBarcode(QString barcodeText);

  explicit ParticipantInfo(QDialog *parent = nullptr);
  ~ParticipantInfo();
private slots:
  //  void SendEmail_clicked();

  void on_pushButtonBack_clicked();

  void on_tableView_activated(const QModelIndex &index);

  void on_pushButtonUpdate_clicked();

  //   void on_pushButtonUploadInfo_clicked();
  void on_pushButtonSendEmail_clicked();

private:
  /** Adds start/check/stop characters to the code */
  QString encodeBarcode(QString code);

  /** Calculates the checksum character from the code */
  int calculateCheckCharacter(QString code);

  int codeToChar(int code);
  int charToCode(int ch);

  bool printerConfigured;
  QPrinter mPrinter;

  Ui::ParticipantInfo *ui;
  CigaraSql sqlObj;
  Smtp smtpObj;
};

#endif // PARTICIPANTINFO_H
