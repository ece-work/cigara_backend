#-------------------------------------------------
#
# Project created by QtCreator 2019-08-05T14:59:10
#
#-------------------------------------------------

QT       += core
QT       += gui
QT       += sql
QT       += widgets
QT       += printsupport
QT += network


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CigaraProject
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11 qt

SOURCES += \
        adminpage.cpp \
        cigarasql.cpp \
        loginupdatepage.cpp \
        main.cpp \
        mainwindow.cpp \
        participantauthentication.cpp \
        participantinfo.cpp \
        printpage.cpp \
        registrationpage.cpp \
        services.cpp \
        smtp.cpp \
        updatepage.cpp

HEADERS += \
    Externals/Sources/libqrencode/qrencode.h \
    adminpage.h \
    cigarasql.h \
    loginupdatepage.h \
    mainwindow.h \
    participantauthentication.h \
    participantinfo.h \
    printpage.h \
    registrationpage.h \
    services.h \
    smtp.h \
    updatepage.h \
    Externals/Sources/QtQrCode \
    Externals/Sources/QtQrCodePainter \
    Externals/Sources/qtqrcode.h \
    Externals/Sources/qtqrcode_global.h \
    Externals/Sources/qtqrcodepainter.h \
    Externals\Sources\libqrencode\qrencode

FORMS += \
    adminpage.ui \
    loginupdatepage.ui \
    mainwindow.ui \
    participantauthentication.ui \
    participantinfo.ui \
    printpage.ui \
    registrationpage.ui \
    updatepage.ui



RESOURCES += \
    cigara_res.qrc

INCLUDEPATH += $$PWD/.
DEPENDPATH += $$PWD/.

INCLUDEPATH += "$$PWD/Externals\Sources"
LIBS += -L"$$PWD/Externals/QrLib" -lqtqrcode

