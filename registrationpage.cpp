#include "registrationpage.h"
#include "ui_registrationpage.h"
#include <QMessageBox>
#include <QSqlTableModel>
#include <QtSql>
#include <updatepage.h>

RegistrationPage::RegistrationPage(QDialog *parent)
    : QDialog(parent), ui(new Ui::RegistrationPage) {
  ui->setupUi(this);
}

RegistrationPage::~RegistrationPage() { delete ui; }

void RegistrationPage::on_pushButtonsubmit_clicked() {
  QSqlQuery query, query1;
  QString email = ui->lineEditEmail->text();
  QString name = ui->lineEditName->text();
  QString surname = ui->lineEditsurname->text();
  QString country = ui->lineEditCountry->text();
  QString passportID = ui->lineEditPassportID->text();
  QString competitor = ui->lineEditCompetitor->text();

  QMap<QString, QString> Participant;
  Participant["email"] = email;
  Participant["name"] = name;
  Participant["surname"] = surname;
  Participant["country"] = country;
  Participant["passportID"] = passportID;
  Participant["competitor"] = competitor;

  query1 = sqlObj.userInfo(email);

  if (name == query1.value(0) && surname == query1.value(1) &&
      email == query1.value(2) && country == query1.value(3) &&
      passportID == query1.value(4)) {
    QMessageBox::information(this, " Registration",
                             "Information is already Present");
  } else {

    if (sqlObj.insertParticipant(Participant)) {
      qDebug() << "inserted to database...";
    } else {
      qDebug() << "not inserted to dtabase...";
    }
  }
}

void RegistrationPage::on_pushButtonBack_clicked() {
  UpdatePage *back = new UpdatePage();
  back->show();
}
