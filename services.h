#ifndef SERVICES_H
#define SERVICES_H

#include <QMessageBox>
#include <QRandomGenerator>
#include <QTime>
class Services
{
public:
    Services();
    static QString getRandomASCII(const qint32 &length, const qint32 &base);

};

#endif // SERVICES_H
