#ifndef ADMINPAGE_H
#define ADMINPAGE_H
#include <QDialog>
#include <participantauthentication.h>
#include <participantinfo.h>
#include <printpage.h>
namespace Ui {
class AdminPage;
}

class AdminPage : public QDialog {
  Q_OBJECT
public:
  explicit AdminPage(QDialog *parent = nullptr);
  ~AdminPage();

private slots:
  void on_pushButtonParticipantInformation_clicked();

  void on_pushButtonDownladExcel_clicked();

  void on_pushButtonParticipantAuthentication_clicked();

  void on_pushButtonPrintInformation_clicked();

  void on_pushButtonLogout_clicked();

private:
  Ui::AdminPage *ui;
  CigaraSql sqlObj;
};

#endif // ADMINPAGE_H
